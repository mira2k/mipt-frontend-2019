import React, { Component } from 'react';
import './styles.css';
import List from './components/tasksList';
import {connect} from 'react-redux';
import {store} from "./index";
import {addTask} from "./actions";

class CreateTask extends Component {
    constructor(props){
        super(props);
        this.state = {
            taskList: [],
            Name: ''
        };
    }

    handleSubmit(event) {
        event.preventDefault();
        console.log("New item is pushed. Task name: ", this.state.Name);
        if (!(this.state.Name.trim() === "")){
            let currentTaskList = this.state.taskList;
            currentTaskList.push(this.state.Name);
            this.props.fetchTaskNum(this.state.Name);
            this.setState({taskList: currentTaskList, Name: ""});

        }
        event.target.reset();
    }

    handleEmailChange(event) {
        console.log("task name", event.target.value);
        this.setState({Name: event.target.value});
    }

    handleCloseTask(id) {
        let currentTaskList = this.state.taskList;
        currentTaskList = currentTaskList.filter(function (item, index) {
            return index !== id
        });
        this.setState({taskList: currentTaskList});
        console.log("task removed. CUrr num", this.state.taskList.length);
    }

    render() {
        return <div>
            <div className={"flex-column"}>
                <form onSubmit={this.handleSubmit.bind(this)} className="flex-column">
                    <input type={"text"}
                           placeholder={"Let's celebrate and add some task!"}
                           value={this.state.email}
                           onChange={this.handleEmailChange.bind(this)}
                           className={"input-line"}
                    />
                    <button className={"button-basic"}> Submit!</button>
                </form>
            </div>
            <h1 className={"flex-column"}>
                These are your tasks!
            </h1>
            <div>
                <h2 className={"flex-column"}> Last added task: {store.getState().reducer.last_task_added}</h2>
            </div>
            <div>
                <List items={this.state.taskList} removeTask={this.handleCloseTask.bind(this)}/>
            </div>
        </div>;
    }
}

export default connect(
    state => ({last_task_added: state.last_task_added
    }),
    dispatch => ({
        fetchTaskNum(title) {
            dispatch(addTask(title));
        }
    })
)(CreateTask);
