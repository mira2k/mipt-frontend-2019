import React, { Component } from 'react';


class OneTask extends Component {
    constructor(props) {
        super(props);

        this.onClickClose = this.onClickClose.bind(this);
    }
    onClickClose() {
        this.props.removeTask(parseInt(this.props.index));
    }
    render() {
        return (
            <div className={"one-skill grey-border"}>
                <div className={"flex-item"}>
                    {this.props.name}
                </div>
                <button type={"button"}
                        onClick={this.onClickClose}
                >X</button>
            </div>
        )
    }
}

export default OneTask;