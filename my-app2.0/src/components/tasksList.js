import OneTask from'./listItem';
import React, { Component } from 'react';

class List extends Component {
    render() {
        return (
            <div>
                <ul>
                {
                    this.props.items.map((item, index) =>
                        <OneTask
                            key={index} index={index}
                            name = {item}
                            removeTask = {this.props.removeTask}
                        />
                    )
                }
            </ul>
            </div>

        );
    }
}

export default List;