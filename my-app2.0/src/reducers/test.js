const initialState = {
    last_task_added: ''
};

const testReducer = (state = initialState, action) => {
    switch(action.type) {
        case 'ADD_TASK':
            return {...state, ...action.payload};

            default:
                return state;
    }
};

export default testReducer;