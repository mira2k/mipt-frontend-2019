import React, { Component } from 'react';
import './styles.css';


class OneTask extends Component {
    constructor(props) {
        super(props);

        this.onClickClose = this.onClickClose.bind(this);
    }
    onClickClose() {
        this.props.removeTask(parseInt(this.props.index));
    }
    render() {
        return (
            <div className={"one-skill grey-border"}>
                <div className={"flex-item"}>
                {this.props.name}
                </div>
                <button type={"button"}
                        onClick={this.onClickClose}
                >X</button>
            </div>
        )
    }
}

class List extends Component {
    render() {
        return (
            <ul>
                {
                    this.props.items.map((item, index) =>
                        <OneTask
                            key={index} index={index}
                            name = {item}
                            removeTask = {this.props.removeTask}
                        />
                    )
                }
            </ul>
        );
    }
}


class CreateTask extends Component {
    constructor(props){
        super(props);
        this.state = {
            taskList: [],
            Name: ''
        };
    }

    handleSubmit(event) {
        event.preventDefault();
        console.log("New item is pushed. Task name: ", this.state.Name);
        if (!(this.state.Name.trim() === "")){
            let currentTaskList = this.state.taskList;
            currentTaskList.push(this.state.Name);
            this.setState({taskList: currentTaskList, Name: ""});
        }
        event.target.reset();
    }

    handleEmailChange(event) {
        console.log("task name", event.target.value);
        this.setState({Name: event.target.value});
    }

    handleCloseTask(id) {
        let currentTaskList = this.state.taskList;
        currentTaskList = currentTaskList.filter(function (item, index) {
            return index !== id
        });
        this.setState({taskList: currentTaskList});
    }

    render() {
        return <div>
            <div className={"flex-column"}>
                <form onSubmit={this.handleSubmit.bind(this)} className="flex-column">
                    <input type={"text"}
                           placeholder={"Let's celebrate and add some task!"}
                           value={this.state.email}
                           onChange={this.handleEmailChange.bind(this)}
                           className={"input-line"}
                    />
                    <button className={"button-basic"}> Submit!</button>
                </form>
            </div>
            <h1 className={"flex-column"}>
                These are your tasks!
            </h1>

            <div>
                <List items={this.state.taskList} removeTask={this.handleCloseTask.bind(this)}/>
            </div>
        </div>;
    }
}

export default CreateTask;
